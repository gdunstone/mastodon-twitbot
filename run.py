#!/usr/bin/env python3
import requests
import random
import string
import os
import time
import mastodon
import tweepy
import yaml
import sys
import traceback
import hashlib
import dbm
import schedule
import threading
import html

config_path = os.environ.get("CONFIG", "config.yaml")

config = yaml.load(open(config_path), Loader=yaml.FullLoader)


def get_file_checksum(fn):
    with open(fn, 'rb') as f:
        return hashlib.md5(f.read()).digest()


def get_str_checksum(s):
    return hashlib.md5(s.encode()).digest()


def get_twitter_users():
    usrs = []
    for handle in config.keys():
        # get the user id
        user = twapi.get_user(handle)
        usrs.append(user)
    return usrs


def get_media(url, force_ext=None):
    def rch():
        return random.choice(string.ascii_letters)
    random_prefix = "".join([rch() for _ in range(6)])
    lastchunk = url.split('/')[-1]
    fn = f"{random_prefix}.{lastchunk.split('.')[-1]}"
    if force_ext:
        fn = f"{random_prefix}.{force_ext}"
    # print(f"temporarily saving {url} in {fn}...")
    response = requests.get(url)
    if response.status_code == 200:
        with open(fn, 'wb') as f:
            f.write(response.content)
    return fn


def try_remove(fn):
    try:
        os.remove(fn)
    except Exception:
        pass


def update_profile(user_dict):
    files = []
    try:
        user = twapi.get_user(user_dict.get("id_str"))
        # authenticate to Mastodon.
        masto = mastodon.Mastodon(
            access_token=config_byid[user.id_str]['mastodon_access_token'],
            api_base_url=os.environ.get("MASTODON_API_BASE_URL")
        )
        print(f"@{masto.me().acct}:login\t\t->\tLogged into Mastodon for profile update")

        with dbm.open('/data/cache.db', 'c') as db:
            try:
                if not len(user.name) > 30:
                    csum = get_str_checksum(user.name)
                    cachepath = f"{masto.me().acct}/display_name".encode()
                    if db.get(cachepath) != csum:
                        db[cachepath] = csum
                        print(f"@{masto.me().acct}:display_name\t->\t{user.name}")
                        masto.account_update_credentials(display_name=user.name)
            except Exception:
                traceback.print_exc()

            try:

                if hasattr(user, "description"):
                    csum = get_str_checksum(user.description)
                    cachepath = f"{masto.me().acct}/description".encode()
                    if db.get(cachepath) != csum:
                        db[cachepath] = csum
                        print(f"@{masto.me().acct}:description\t->\t{user.description}")
                        masto.account_update_credentials(
                            note=f"I am a Twitbot\n{user.description}"
                        )
            except Exception:
                traceback.print_exc()

            try:
                url = user.profile_image_url
                fn = get_media(url)
                files.append(fn)
                csum = get_file_checksum(fn)
                cachepath = f"{masto.me().acct}/avatar".encode()
                if db.get(cachepath) != csum:
                    db[cachepath] = csum
                    print(f"@{masto.me().acct}:avatar\t\t->\t{fn}")
                    masto.account_update_credentials(avatar=fn)

            except Exception:
                traceback.print_exc()

            if user._json.get("profile_banner_url"):
                try:
                    banner_url = user._json.get("profile_banner_url")
                    bfn = get_media(banner_url + "/1500x500", force_ext="jpg")
                    files.append(bfn)

                    csum = get_file_checksum(bfn)
                    cachepath = f"{masto.me().acct}/header".encode()
                    if db.get(cachepath) != csum:
                        db[cachepath] = csum
                        print(f"@{masto.me().acct}:header\t\t->\t{bfn}")
                        masto.account_update_credentials(header=bfn)
                except Exception:
                    traceback.print_exc()
        print(f"@{masto.me().acct}:login\t\t->\tProfile update complete")
    except Exception:
        traceback.print_exc()
    finally:
        for file in files:
            try_remove(file)


class TwitterStreamListener(tweepy.StreamListener):
    def on_status(self, tweet: tweepy.Status):
        try:
            # no retweets or replies
            if hasattr(tweet, "retweeted_status") or tweet.in_reply_to_status_id_str or tweet.is_quote_status:
                return

            if hasattr(tweet, 'extended_tweet'):
                print(f"{tweet.id_str} - {tweet.user.screen_name}: extended tweet with full text")
                text = tweet.extended_tweet['full_text']
            else:
                text = tweet.text
            # unescape the html
            text = html.unescape(text)

            if not config.get(tweet.user.screen_name):
                print(f"no user {tweet.user.screen_name} in config")
                return
            print(tweet)

            print("Logging into Mastodon to toot a tweet")
            # authenticate to Mastodon.
            masto = mastodon.Mastodon(
                access_token=config_byid[tweet.user.id_str]['mastodon_access_token'],
                api_base_url=os.environ.get("MASTODON_API_BASE_URL")
            )

            for url in tweet.entities.get("urls", []):
                try:
                    print(f"Replacing url {url['url']} with {url['expanded_url']}")
                    text = text.replace(url['url'], url['expanded_url'])
                except Exception:
                    print(f"error replacing url, {url}")
                    if os.environ.get("DEBUG") in ("true", "1", 1):
                        traceback.print_exc()
            files = []
            try:
                media_ids = []
                medias = None
                try:
                    medias = tweet.extended_entities["media"]
                except Exception:
                    print("no extended_entities attr, using entities")
                    medias = tweet.entities["media"]
                if len(medias) > 0:
                    for media in medias:
                        if media['type'] in ["photo", "video"]:
                            try:
                                url = media['media_url_https']
                                fn = get_media(url)
                                print(f"posting media {fn} to Mastodon")
                                m_media = masto.media_post(fn, description=text)
                                media_ids.append(m_media)
                                time.sleep(0.2)
                                os.remove(fn)
                            except Exception as e:
                                print(f"Exception getting or posting media: {e}")
            except Exception:
                traceback.print_exc()
            finally:
                for file in files:
                    try_remove(file)
            status = masto.status_post(
                text,
                media_ids=media_ids,
                visibility=os.environ.get("MASTODON_VISIBILITY", "unlisted")
            )
            print(f"@{masto.me().acct} posted to Mastodon: {status.url}")
        except Exception:
            traceback.print_exc()

    def on_error(self, status_code):
        print(f"twitbot error code: {status_code}")

        if status_code == 420:
            # returning False in on_error disconnects the stream
            return False


print("Logging into Twitter")
# setup Twitter auth
twauth = tweepy.OAuthHandler(
    os.environ.get("TWITTER_CONSUMER_KEY"),
    os.environ.get("TWITTER_CONSUMER_SECRET")
)
twauth.set_access_token(
    os.environ.get("TWITTER_ACCESS_TOKEN"),
    os.environ.get("TWITTER_ACCESS_TOKEN_SECRET")
)

# authenticate to Twitter
twapi = tweepy.API(
    twauth,
    wait_on_rate_limit=True,
    wait_on_rate_limit_notify=True,
)
config_byid = {}
for handle in config.keys():
    user = twapi.get_user(handle)
    config[handle]['id_str'] = user.id_str
    config_byid[user.id_str] = config[handle]

userids = [user['id_str'] for user in config.values()]
x = 0


def update_profiles():
    print("Updating profiles")
    for user in config.values():
        update_profile(user)


def run_threaded(job_func):
    job_thread = threading.Thread(target=job_func, daemon=True)
    job_thread.start()


schedule.every(24).hours.do(run_threaded, update_profiles)

run_threaded(update_profiles)

try:
    print(f"Streaming from accounts: {','.join(config.keys())}")
    twstream = tweepy.Stream(auth=twapi.auth, listener=TwitterStreamListener())
    twstream.filter(follow=userids)
except KeyboardInterrupt:
    print("exiting")
except Exception:
    traceback.print_exc()
finally:
    print("Disconnecting...")
    twstream.disconnect()
    sys.exit(1)
